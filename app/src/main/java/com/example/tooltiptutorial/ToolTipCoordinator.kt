package com.example.tooltiptutorial

import android.view.View
import com.example.tooltiptutorial.model.IToolTipCoordinator
import com.example.tooltiptutorial.model.Tip

/**
 * ToolTipCoordinator es una implementacion de IToolTipCoordinator, implementa la logica
 * de "siguiente" en el tooltipview
 */
class ToolTipCoordinator(val tips : List<Tip>) : IToolTipCoordinator {

    var index = 0

    override fun getNextTip(): Tip? {
        if (index == tips.size) return null

        val result  = tips[index]
        index ++
        return result
    }
}