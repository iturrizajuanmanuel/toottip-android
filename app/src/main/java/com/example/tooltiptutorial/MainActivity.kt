package com.example.tooltiptutorial

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.MenuItemCompat
import com.example.tooltiptutorial.model.Tip

class MainActivity : AppCompatActivity() {

    lateinit var menuView : View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.main_menu, menu)
        menuView = menu!!.getItem(0).actionView
        initFromGenerator()
        return super.onCreateOptionsMenu(menu)
    }



    override fun onStart() {
        super.onStart()
        //initFromGenerator()

        setSupportActionBar( findViewById(R.id.toolbar))
        /*
        var index = 0
        val viewsList : List<View> = listOf(findViewById(R.id.textView), findViewById(R.id.button), findViewById(R.id.texto2))

        val tooltip = ToolTipView(this, null)

            button.setOnClickListener {

                if (index == 0) view.addView(tooltip)

                if (index < viewsList.size){
                    tooltip.highlight(viewsList[index])
                    index ++
                }
        }

        button.setOnLongClickListener {
            view.removeView(tooltip)
            true
        } */

    }

    private fun initFromGenerator(){
        val view = findViewById<ConstraintLayout>(R.id.container)
        val button = findViewById<Button>(R.id.button)

        val viewsList : List<View> = listOf(findViewById(R.id.textView), findViewById(R.id.button), findViewById(R.id.texto2))

        val tips = listOf(
            Tip("Hitn 1", viewsList[0]),
            Tip("Hitn 2", viewsList[1]),
            Tip("Hitn 3", viewsList[2])
        )

        val generator = ToolTipGenerator(ToolTipCoordinator(tips), view)

        button.setOnClickListener {
         generator.nextTip()
        }
    }

}