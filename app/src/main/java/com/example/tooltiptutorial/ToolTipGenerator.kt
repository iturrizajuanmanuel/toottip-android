package com.example.tooltiptutorial

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.tooltiptutorial.model.IToolTipCoordinator
import com.example.tooltiptutorial.view.FocusView
import com.example.tooltiptutorial.view.ITipView
import com.example.tooltiptutorial.view.TipViewToast

/**
 * TooltipGenerator es responsable de instanciar ToolTipView, HintView y avisar al coordinator
 * las acciones del usuario
 */
class ToolTipGenerator(val coordinator: IToolTipCoordinator, containerView : View) {

    private lateinit var focusView : FocusView

    var tipView : ITipView = TipViewToast() //Toast por default

    init {
        (containerView as? ViewGroup)?.let { containerView -> createView(containerView) }
    }

    private fun createView(containerView: ViewGroup) {
        val inflater = LayoutInflater.from(containerView.context)
        val inflatedLayout = inflater.inflate(R.layout.tool_tip_layout,containerView,true)

        focusView = inflatedLayout.findViewById(R.id.focus_view)
        focusView.bringToFront()

        inflatedLayout.findViewById<Button>(R.id.skip_btn).setOnClickListener {
            stopShowingTooltips()
        }

    }

    fun stopShowingTooltips(){
        (focusView.parent as ViewGroup).removeView(focusView)
    }

    fun nextTip(){
        coordinator.getNextTip()?.let {
            focusView.highlight(it.viewToFocus)
            tipView.showTip(it.viewToFocus, it.hint)
        }

    }

}