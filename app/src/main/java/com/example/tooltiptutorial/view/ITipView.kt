package com.example.tooltiptutorial.view

import android.view.View

/**
 * La interfaz ITipView define el comportamiento necesario
 * para mostrar un hint en pantalla
 */
interface ITipView {

    fun showTip(view: View, message:String)

}