package com.example.tooltiptutorial.view

import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator

class FocusView(context: Context, attrs: AttributeSet? = null) : View(context, attrs) {

     var borderColor : Int = android.R.color.holo_blue_bright

    //anchors de la view
    var left : Float = 0f
    var right : Float = 0f
    var top : Float = 0f
    var bottom : Float = 0f
        //TODO: cambiar el evento donde hago el invalidate()
        set(value) {
            field = value
            invalidate()
        }

    //animators
    val leftAnimator = ObjectAnimator.ofFloat(this, "left", 0f,0f)
    val rightAnimator = ObjectAnimator.ofFloat(this, "right", 0f,0f)
    val topAnimator = ObjectAnimator.ofFloat(this, "top", 0f,0f)
    val bottomAnimator = ObjectAnimator.ofFloat(this, "bottom", 0f,0f)

    //draw constants
    val backgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        this.color = Color.DKGRAY;
        this.alpha = 200
    }
    val cleanPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        this.color = Color.TRANSPARENT
        this.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_OUT)
    }

    fun highlight(selectedView : View) {
        leftAnimator.setFloatValues(left , selectedView.x)
        rightAnimator.setFloatValues(right , selectedView.x + selectedView.width)
        topAnimator.setFloatValues(top, selectedView.y)
        bottomAnimator.setFloatValues(bottom , selectedView.y + selectedView.height)

        listOf(leftAnimator, rightAnimator, topAnimator, bottomAnimator).forEach {
            it.start()
        }
    }

    init {
        setLayerType(LAYER_TYPE_HARDWARE, null) //necesario para la transparencia

        listOf(leftAnimator, rightAnimator, topAnimator, bottomAnimator).forEach {
            it.duration = 400
            it.interpolator = AccelerateDecelerateInterpolator()
        }

    }

    override fun draw(canvas: Canvas?) {
        super.draw(canvas)
        canvas?.drawRect(rootView.x, rootView.y, rootView.x + rootView.width, rootView.y + rootView.height, backgroundPaint)

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            canvas?.drawRoundRect(left, top, right, bottom, 8f, 8f, cleanPaint)
        } else {
            canvas?.drawRect(left,top,right, bottom, cleanPaint)
        }

    }

}