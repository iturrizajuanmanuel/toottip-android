package com.example.tooltiptutorial.view

import android.view.View
import android.widget.Toast

class TipViewToast : ITipView {

    override fun showTip(view: View, message: String) {
        Toast.makeText(view.context, message, Toast.LENGTH_SHORT).show()
    }

}