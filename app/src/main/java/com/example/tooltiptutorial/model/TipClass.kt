package com.example.tooltiptutorial.model

enum class TipClass{
        VIEW, MENU_ITEM
}