package com.example.tooltiptutorial.model

import android.view.View

interface IToolTipCoordinator {

    fun getNextTip() : Tip?

}
