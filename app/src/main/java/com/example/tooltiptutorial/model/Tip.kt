package com.example.tooltiptutorial.model

import android.view.View

data class Tip(val hint: String, val viewToFocus : View, val id : String = "")